<?php

/**
 * Fired during plugin activation
 *
 * @link       https://codeable.io/developers/david-towoju/
 * @since      1.0.0
 *
 * @package    Wbmr
 * @subpackage Wbmr/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wbmr
 * @subpackage Wbmr/includes
 * @author     David Towoju (Figarts) <hello@figarts.co>
 */
class Wbmr_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
