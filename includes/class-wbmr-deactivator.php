<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://codeable.io/developers/david-towoju/
 * @since      1.0.0
 *
 * @package    Wbmr
 * @subpackage Wbmr/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wbmr
 * @subpackage Wbmr/includes
 * @author     David Towoju (Figarts) <hello@figarts.co>
 */
class Wbmr_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
