<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://codeable.io/developers/david-towoju/
 * @since      1.0.0
 *
 * @package    Wbmr
 * @subpackage Wbmr/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Wbmr
 * @subpackage Wbmr/includes
 * @author     David Towoju (Figarts) <hello@figarts.co>
 */
class Wbmr {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Wbmr_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'WBMR_VERSION' ) ) {
			$this->version = WBMR_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'wbmr';

		$this->load_dependencies();
		$this->set_locale();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Wbmr_Loader. Orchestrates the hooks of the plugin.
	 * - Wbmr_i18n. Defines internationalization functionality.
	 * - Wbmr_Admin. Defines all hooks for the admin area.
	 * - Wbmr_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wbmr-i18n.php';

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Wbmr_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Wbmr_i18n();
		add_action( 'plugins_loaded', array($plugin_i18n, 'load_plugin_textdomain') );

	}


	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

    wp_enqueue_style( $this->plugin_name, WBMRURL . 'assets/css/wbmr-public.css', array(), $this->version, 'all' );

    wp_enqueue_style( 'select2', WBMRURL . 'assets/css/select2.css', array(), $this->version, 'all' );

		wp_enqueue_script( 'select2', WBMRURL . 'assets/js/select2.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, WBMRURL . 'assets/js/wbmr-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
    add_filter( 'woocommerce_locate_template', array($this, 'woocommerce_locate_template'), 10, 3 );
		add_action( 'wp_enqueue_scripts', array($this, 'enqueue_scripts'), 1 );
		add_action( 'init', array($this, 'initialize') );
	}

  public function initialize()
  {
    remove_all_actions('wp_ajax_wc_bookings_calculate_costs');
    remove_all_actions('wp_ajax_nopriv_wc_bookings_calculate_costs');
    $this->remove_filters_with_method_name( 'woocommerce_add_to_cart_validation', 'validate_add_cart_item', 10 );
    $this->remove_filters_with_method_name( 'woocommerce_add_cart_item_data', 'add_cart_item_data', 10 );

    if ( version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
      $this->remove_filters_with_method_name( 'woocommerce_new_order_item', 'order_item_meta', 10 );
		} else {
      $this->remove_filters_with_method_name( 'woocommerce_add_order_item_meta', 'order_item_meta', 10 );
		}

		add_action( 'wp_ajax_wc_bookings_calculate_costs', array( $this, 'calculate_costs' ) );
		add_action( 'wp_ajax_nopriv_wc_bookings_calculate_costs', array( $this, 'calculate_costs' ) );

		add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'validate_add_cart_item' ), 10, 3 );

    add_filter( 'woocommerce_add_cart_item_data', array( $this, 'add_cart_item_data' ), 10, 2 );

		if ( version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
			add_action( 'woocommerce_new_order_item', array( $this, 'order_item_meta' ), 50, 2 );
		} else {
			add_action( 'woocommerce_add_order_item_meta', array( $this, 'order_item_meta' ), 50, 2 );
		}

  }

	/**
	 * Calculate costs.
	 *
	 * Take posted booking form values and then use these to quote a price for what has been chosen.
	 * Returns a string which is appended to the booking form.
	 */
	public function calculate_costs() {
		$posted = array();

		parse_str( $_POST['form'], $posted );

		$booking_id = $posted['add-to-cart'];
		$product    = wc_get_product( $booking_id );

		if ( ! $product ) {
			wp_send_json( array(
				'result' => 'ERROR',
				'html'   => apply_filters( 'woocommerce_bookings_calculated_booking_cost_error_output', '<span class="booking-error">' . __( 'This booking is unavailable.', 'woocommerce-bookings' ) . '</span>', null, null ),
			) );
		}

		// figarts
    // dump($posted['wc_bookings_field_resource']);
    if($posted['wc_bookings_field_resource']){
      foreach ($posted['wc_bookings_field_resource'] as $key => $value) {

        $booking_form     = new WC_Booking_Form( $product );
        $_posted = $posted;
        $_posted['wc_bookings_field_resource'] = absint( $value );
        $cost             = $booking_form->calculate_booking_cost( $_posted );
        // var_dump($booking_form);
        if ( is_wp_error( $cost ) ) {
          wp_send_json( array(
            'result' => 'ERROR',
            'html'   => apply_filters( 'woocommerce_bookings_calculated_booking_cost_error_output', '<span class="booking-error">' . $cost->get_error_message() . '</span>', $cost, $product ),
          ) );
        }

        $tax_display_mode = get_option( 'woocommerce_tax_display_shop' );

        if ( 'incl' === get_option( 'woocommerce_tax_display_shop' ) ) {
          if ( function_exists( 'wc_get_price_excluding_tax' ) ) {
            $display_price = wc_get_price_including_tax( $product, array( 'price' => $cost ) );
          } else {
            $display_price = $product->get_price_including_tax( 1, $cost );
          }
        } else {
          if ( function_exists( 'wc_get_price_excluding_tax' ) ) {
            $display_price = wc_get_price_excluding_tax( $product, array( 'price' => $cost ) );
          } else {
            $display_price = $product->get_price_excluding_tax( 1, $cost );
          }
        }

        if ( version_compare( WC_VERSION, '2.4.0', '>=' ) ) {
          $price_suffix = $product->get_price_suffix( $cost, 1 );
        } else {
          $price_suffix = $product->get_price_suffix();
        }
        $_display_price[] = $display_price;
			}

			// Leave block cost only for the first resource when there are multiple resources
			$display_price = array_sum($_display_price);

			// if ( is_wp_error( $display_price ) ) {
			// 	wp_send_json( array(
			// 		'result' => 'ERROR',
			// 		'html'   => apply_filters( 'woocommerce_bookings_calculated_booking_cost_error_output', '<span class="booking-error">' . $display_price->get_error_message() . '</span>', $display_price, $product ),
			// 	) );
			// }

			$duration = $posted['wc_bookings_field_duration'];
			$resource_count = count($posted['wc_bookings_field_resource']);

			if ($resource_count > $duration && $resource_count >= 1){
				// $reduction = $resource_count - $duration;
				// $reduceby = $reduction * $booking_form->product->get_block_cost();
				// $display_price -= $reduceby;
			}


			// Build the output
			$output = apply_filters( 'woocommerce_bookings_booking_cost_string', __( 'Booking cost', 'woocommerce-bookings' ), $product ) . ': <strong>' . wc_price( $display_price ) . $price_suffix . '</strong>';

			// Send the output
			wp_send_json( array(
				'result' => 'SUCCESS',
				'html'   => apply_filters( 'woocommerce_bookings_calculated_booking_cost_success_output', $output, $display_price, $product ),
			) );

    }
    else{
      $booking_form     = new WC_Booking_Form( $product );
			$cost             = $booking_form->calculate_booking_cost( $posted );
			// dump( $cost );

      if ( is_wp_error( $cost ) ) {
        wp_send_json( array(
          'result' => 'ERROR',
          'html'   => apply_filters( 'woocommerce_bookings_calculated_booking_cost_error_output', '<span class="booking-error">' . $cost->get_error_message() . '</span>', $cost, $product ),
        ) );
			}
	
			$tax_display_mode = get_option( 'woocommerce_tax_display_shop' );

			if ( 'incl' === get_option( 'woocommerce_tax_display_shop' ) ) {
				if ( function_exists( 'wc_get_price_excluding_tax' ) ) {
					$display_price = wc_get_price_including_tax( $product, array( 'price' => $cost ) );
				} else {
					$display_price = $product->get_price_including_tax( 1, $cost );
				}
			} else {
				if ( function_exists( 'wc_get_price_excluding_tax' ) ) {
					$display_price = wc_get_price_excluding_tax( $product, array( 'price' => $cost ) );
				} else {
					$display_price = $product->get_price_excluding_tax( 1, $cost );
				}
			}
	
			if ( version_compare( WC_VERSION, '2.4.0', '>=' ) ) {
				$price_suffix = $product->get_price_suffix( $cost, 1 );
			} else {
				$price_suffix = $product->get_price_suffix();
			}

			// Build the output
			$output = apply_filters( 'woocommerce_bookings_booking_cost_string', __( 'Booking cost', 'woocommerce-bookings' ), $product ) . ': <strong>' . wc_price( $display_price ) . $price_suffix . '</strong>';

			// Send the output
			wp_send_json( array(
				'result' => 'SUCCESS',
				'html'   => apply_filters( 'woocommerce_bookings_calculated_booking_cost_success_output', $output, $display_price, $product ),
			) );


    }



	}



	/**
	 * When a booking is added to the cart, validate it
	 *
	 * @param mixed $passed
	 * @param mixed $product_id
	 * @param mixed $qty
	 * @return bool
	 */
	public function validate_add_cart_item( $passed, $product_id, $qty ) {
		$product = wc_get_product( $product_id );

		if ( ! is_wc_booking_product( $product ) ) {
			return $passed;
		}

    // figarts
    $resources = $_POST['wc_bookings_field_resource'];
    foreach ($resources as $resource) {
      $__POST = $_POST;
      $__POST['wc_bookings_field_resource'] = $resource;

      $booking_form = new WC_Booking_Form( $product );
      $data  = $booking_form->get_posted_data($__POST);
      $validate     = $booking_form->is_bookable( $data );
      if ( is_wp_error( $validate ) ) {
        wc_add_notice( $validate->get_error_message(), 'error' );
        return false;
      }
    }
		return $passed;
	}


	/**
	 * Add posted data to the cart item
	 *
	 * @param mixed $cart_item_meta
	 * @param mixed $product_id
	 * @return array $cart_item_meta
	 */
	public function add_cart_item_data( $cart_item_meta, $product_id ) {
    // dump($cart_item_meta);

		$product = wc_get_product( $product_id );

		if ( ! is_wc_booking_product( $product ) ) {
			return $cart_item_meta;
		}

		$resources = $_POST['wc_bookings_field_resource'];

    foreach ($resources as $key => $resource) {

      $__POST = $_POST;
      $__POST['wc_bookings_field_resource'] = $resource;
      // dump($__POST);
      $booking_form                       = new WC_Booking_Form( $product );
      $cart_item_meta['booking']          = $booking_form->get_posted_data( $__POST );

      $cart_item_meta['booking']['_cost'] = $booking_form->calculate_booking_cost( $__POST );

			$_cost[] = $cart_item_meta['booking']['_cost'];
			$_type[] = $cart_item_meta['booking']['type'];

      // Create the new booking
      $new_booking = $this->create_booking_from_cart_data( $cart_item_meta, $product_id );

      // Store in cart
      $cart_item_meta['booking']['_booking_id'] = $new_booking->get_id();
			$_booking_ids[] = $cart_item_meta['booking']['_booking_id'];

      // Schedule this item to be removed from the cart if the user is inactive.
      // $this->schedule_cart_removal( $new_booking->get_id() );
    }
    $cart_item_meta['booking']['_cost'] = array_sum($_cost);
		$cart_item_meta['booking']['type'] = implode(', ', $_type);
    $cart_item_meta['booking']['_wbmr_booking_ids'] = implode(',', $_booking_ids);
		return $cart_item_meta;
	}

  function remove_filters_with_method_name( $hook_name = '', $method_name = '', $priority = 0 ) {
    global $wp_filter;
    // Take only filters on right hook name and priority
    if ( ! isset( $wp_filter[ $hook_name ][ $priority ] ) || ! is_array( $wp_filter[ $hook_name ][ $priority ] ) ) {
        return false;
    }
    // Loop on filters registered
    foreach ( (array) $wp_filter[ $hook_name ][ $priority ] as $unique_id => $filter_array ) {
        // Test if filter is an array ! (always for class/method)
        if ( isset( $filter_array['function'] ) && is_array( $filter_array['function'] ) ) {
            // Test if object is a class and method is equal to param !
            if ( is_object( $filter_array['function'][0] ) && get_class( $filter_array['function'][0] ) && $filter_array['function'][1] == $method_name ) {
                // Test for WordPress >= 4.7 WP_Hook class (https://make.wordpress.org/core/2016/09/08/wp_hook-next-generation-actions-and-filters/)
                if ( is_a( $wp_filter[ $hook_name ], 'WP_Hook' ) ) {
                    unset( $wp_filter[ $hook_name ]->callbacks[ $priority ][ $unique_id ] );
                } else {
                    unset( $wp_filter[ $hook_name ][ $priority ][ $unique_id ] );
                }
            }
        }
    }
    return false;
  }

	/**
	 * Create booking from cart data
	 *
	 * @param        $cart_item_meta
	 * @param        $product_id
	 * @param string $status
	 *
	 * @return WC_Booking
	 */
	private function create_booking_from_cart_data( $cart_item_meta, $product_id, $status = 'in-cart' ) {
		// Create the new booking
		$new_booking_data = array(
			'product_id'    => $product_id, // Booking ID
			'cost'          => $cart_item_meta['booking']['_cost'], // Cost of this booking
			'start_date'    => $cart_item_meta['booking']['_start_date'],
			'end_date'      => $cart_item_meta['booking']['_end_date'],
			'all_day'       => $cart_item_meta['booking']['_all_day'],
		);

    // Check if the booking has resources
		if ( isset( $cart_item_meta['booking']['_resource_id'] ) ) {
			$new_booking_data['resource_id'] = $cart_item_meta['booking']['_resource_id']; // ID of the resource
		}

		// Checks if the booking allows persons
		if ( isset( $cart_item_meta['booking']['_persons'] ) ) {
			$new_booking_data['persons'] = $cart_item_meta['booking']['_persons']; // Count of persons making booking
		}

		$new_booking = get_wc_booking( $new_booking_data );
		$new_booking->create( $status );

		return $new_booking;
	}

	/**
	 * order_item_meta function
	 *
	 * @param mixed $item_id
	 * @param mixed $values
	 */
	public function order_item_meta( $item_id, $values ) {

		if ( ! empty( $values['booking'] ) ) {
			$product        = $values['data'];
			$booking_id     = $values['booking']['_booking_id'];
			$_resource_ids     = $values['booking']['_wbmr_booking_ids'];
		}

		if ( ! isset( $booking_id ) && ! empty( $values->legacy_values ) && is_array( $values->legacy_values ) && ! empty( $values->legacy_values['booking'] ) ) {
			$product        = $values->legacy_values['data'];
			$booking_id     = $values->legacy_values['booking']['_booking_id'];
			$_resource_ids     = $values->legacy_values['booking']['_wbmr_booking_ids'];
    }

    if ($_resource_ids){
      $_resource_ids = explode(',', $_resource_ids);
    }

    // dump(explode(',', $_resource_ids));
    // wp_dump($values->legacy_values['booking']);
		if ( isset( $booking_id ) ) {
			// figarts
			foreach ($_resource_ids as $booking_id) {
				# code...
				$booking        = get_wc_booking( $booking_id );
				$booking_status = 'unpaid';

				if ( function_exists( 'wc_get_order_id_by_order_item_id' ) ) {
					$order_id = wc_get_order_id_by_order_item_id( $item_id );
				} else {
					global $wpdb;
					$order_id = (int) $wpdb->get_var( $wpdb->prepare(
						"SELECT order_id FROM {$wpdb->prefix}woocommerce_order_items WHERE order_item_id = %d",
						$item_id
					) );
				}

				// Set as pending when the booking requires confirmation
				if ( wc_booking_requires_confirmation( $values['product_id'] ) ) {
					$booking_status = 'pending-confirmation';
				}

				$booking->set_order_id( $order_id );
				$booking->set_order_item_id( $item_id );
				$booking->set_status( $booking_status );
				$booking->save();

			}

		}
	}



  function woocommerce_locate_template( $template, $template_name, $template_path ) {

    if ( !in_array( $template_name, array('booking-form/select.php') ) )
      return $template;

    global $woocommerce;

    $_template = $template;

    if ( ! $template_path ) $template_path = $woocommerce->template_url;

    $plugin_path  =  WBMRPATH . '/woocommerce-bookings/';

    // Look within passed path within the theme - this is priority
    $template = locate_template(
      array(
        $template_path . $template_name,
        $template_name
      )
    );

    // Modification: Get the template from this plugin, if it exists
    if ( ! $template && file_exists( $plugin_path . $template_name ) )
      $template = $plugin_path . $template_name;

    // Use default template
    if ( ! $template )
      $template = $_template;

    // Return what we found
    return $template;
  }



	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Wbmr_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}





















}
