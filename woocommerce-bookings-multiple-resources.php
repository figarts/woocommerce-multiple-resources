<?php

/**
 * @link              https://codeable.io/developers/david-towoju/
 * @since             1.0.0
 * @package           Wbmr
 *
 * @wordpress-plugin
 * Plugin Name:       WooCommerce Bookings Multiple Resources
 * Plugin URI:        https://figarts.co
 * Description:       Adds multiple bookings to WooCommerce
 * Version:           1.2
 * Author:            David Towoju (Figarts)
 * Author URI:        https://figarts.co
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wbmr
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WBMRVERSION', '1.2' );
define( 'WBMRPATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'WBMRURL', plugin_dir_url( __FILE__ ));

require_once plugin_dir_path( __FILE__ ) . 'includes/class-wbmr-license.php';
WBMR_License_Menu::instance( __FILE__, 'WBMR', WBMRVERSION, 'plugin', 'https://figarts.co/' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wbmr-activator.php
 */
function activate_wbmr() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wbmr-activator.php';
	Wbmr_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wbmr-deactivator.php
 */
function deactivate_wbmr() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wbmr-deactivator.php';
	Wbmr_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wbmr' );
register_deactivation_hook( __FILE__, 'deactivate_wbmr' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wbmr.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wbmr() {

	$plugin = new Wbmr();
	$plugin->run();

}
run_wbmr();
