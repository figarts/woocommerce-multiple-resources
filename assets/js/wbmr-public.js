(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 */

  $(function() {

		$(".js-select2").select2({
			closeOnSelect : false,
			placeholder : "Select resources",
			allowHtml: true,
			// allowClear: true,
			tags: true // создает новые опции на лету
		});

    $('.wc-bookings-booking-form')
		.on('change', 'input, select', function( e ) {
      var name  = $(this).attr('name');
      var $form = $(this).closest('form');
			var isEmptyCalendarSelection =  ! $form.find( "[name='wc_bookings_field_start_date_day']" ).val() &&
        ! $form.find( '#wc_bookings_field_start_date' ).val();

			if ( 'wc_bookings_field_resource[]' === name && isEmptyCalendarSelection ) {
				wc_bookings_date_picker.refresh_datepicker();
				return;
			}
    });



  });
})( jQuery );
